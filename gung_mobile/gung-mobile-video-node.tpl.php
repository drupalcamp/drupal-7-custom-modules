    <div id="tabs_container">
      <ul class="tabs twelve mobile-twelve columns">
        <li class="active four mobile-four columns">
          <a href="#tab_1_contents" rel="#tab_1_contents" class="tab active" onclick="return false"><?php print t('Related <br> Videos'); ?></a>
        </li>
        <li class="four mobile-four columns"><a href="#tab_2_contents" rel="#tab_2_contents" class="tab" onclick="return false"><?php print t('More From <br> This Expert'); ?></a></li>
        <li class="four mobile-four columns"><a href="#tab_3_contents" rel="#tab_3_contents" class="tab" onclick="return false"><?php print t('Expert Bio <br> & Rescources'); ?></a></li>
      </ul>
      <div class="clear"></div>
      <div class="tab_contents_container">
        <div id="tab_1_contents" class="tab_contents tab_contents_active">
          <?php print render($variables['related_videos']); ?>
        </div>
        <div id="tab_2_contents" class="tab_contents">
          <?php
            // print render($variables['more_videos']['content']);
          print render($variables['more_videos']);
          ?>
        </div>
        <div id="tab_3_contents" class="tab_contents">
          <?php print render($variables['expert_bio']);?>
        </div>

      </div>
    </div>


