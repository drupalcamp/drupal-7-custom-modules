<?php
function gung_mobile_admin_settings_form($form, &$form_state) {
  $form['gung_mobile_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('GUNG Mobile Redirect Settings'),
      '#description' => t('Links for mobile redirects'),
  );
  $form['gung_mobile_settings']['gung_mobile_desktop_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL for Desktop'),
    '#default_value' => variable_get('gung_mobile_desktop_url', '/'),
      '#size' => 40,
      '#maxlength' => 255,
      '#required' => TRUE,
    '#description' => t('Enter the desktop URL'),
  );
  $form['gung_mobile_settings']['gung_mobile_desktop_url_2'] = array(
    '#type' => 'textfield',
    '#title' => t('2nd URL for Desktop (optional)'),
    '#default_value' => variable_get('gung_mobile_desktop_url_2', '/'),
      '#size' => 40,
      '#maxlength' => 255,
      '#required' => TRUE,
    '#description' => t('Enter the desktop URL'),
  );
    $form['gung_mobile_settings']['gung_mobile_mobile_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL for Mobile Devices'),
    '#default_value' => variable_get('gung_mobile_mobile_url', '/'),
      '#size' => 40,
      '#maxlength' => 255,
      '#required' => TRUE,
    '#description' => t('Enter the URL for the mobile site'),
  );
  return system_settings_form($form);
}
