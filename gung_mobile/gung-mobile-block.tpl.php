
<section id="sidebar" role="complementary">

  <?php
    // print l('Sign In', 'user', array('attributes' => array('class' => 'button', 'data-transition' => 'slide')));
    // print '<div class="create-account">'. l('Create Account', 'gung-register/nojs/form'). '</div>';
    global $base_path;
    print '<div class="login-button">'. ctools_modal_text_button(t('SIGN IN'), 'ajax_register/login/nojs', t('Sign In'), 'ctools-modal-ctools-ajax-register-style'). '</div>';
    print '<div class="create-account">'. ctools_modal_text_button(t('Create Account'), 'gung-register/nojs/form', t('Create Account'), 'ctools-modal-ctools-ajax-register-style'). '</div>';
    print '<div class="logout-button">'. l(t('LOGOUT'), $base_url. 'user/logout'). '</div>';
    print '<div class="menu-title">'. $variables['menu_nav']['subject']. '</div>'. render($variables['menu_nav']['content']);
    // print '<div class="menu-title">'. $variables['menu_trend']['subject']. '</div>'. render($variables['menu_trend']['content']);
    print '<div class="menu-title">'. $variables['menu_category']['subject']. '</div>'. render($variables['menu_category']['content']);
  ?>
</section>
