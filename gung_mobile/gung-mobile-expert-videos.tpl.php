    <div id="tabs_container">
      <ul class="tabs twelve mobile-twelve columns">
        <li class="six mobile-six columns"><a href="#tab_1_contents" rel="#tab_1_contents" class="tab active" onclick="return false"><?php print t('My Videos'); ?></a></li>
        <li class="six mobile-six columns"><a href="#tab_2_contents" rel="#tab_2_contents" class="tab" onclick="return false"><?php print t('Expert Bio & Rescources'); ?></a></li>
      </ul>
      <div class="clear"></div>
      <div class="tab_contents_container">
        <div id="tab_1_contents" class="tab_contents tab_contents_active">
          <?php
          print render($variables['my_videos']);
          ?>
        </div>
        <div id="tab_2_contents" class="tab_contents">
          <?php print render($variables['expert_bio']);?>
        </div>
      </div>
    </div>


