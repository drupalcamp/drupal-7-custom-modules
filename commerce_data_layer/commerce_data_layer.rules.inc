<?php
/**
 * @file
 * Rules integration file
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_data_layer_rules_action_info() {
  return array(
    // Add rules action which sends the transaction code.
    'commerce_data_layer_send_order' => array(
      'label' => t('Send order to data layer for GA through GTM'),
      'parameter' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('Order in checkout'),
        ),
      ),
      'callback' => array(
        'execute' => 'commerce_data_layer_send_order',
      ),
    ),

  'commerce_data_layer_add_to_cart' => array(
    'label' => t('Send add to cart GA through GTM'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_product',
        'label' => t('Product Added'),
      ),
    ),
    'callback' => array(
      'execute' => 'commerce_data_layer_add_to_cart',
    ),
    ),
   'commerce_data_layer_remove_from_cart' => array(
     'label' => t('Send remove cart data layer for GA through GTM'),
     'parameter' => array(
       'commerce_product' => array(
         'type' => 'commerce_product',
         'label' => t('Product Removed'),
       ),
     ),
     'callback' => array(
       'execute' => 'commerce_data_layer_remove_from_cart',
     ),
    ),

  );
}