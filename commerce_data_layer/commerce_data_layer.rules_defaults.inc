<?php
/**
 * @file
 * Rules integration file
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_data_layer_default_rules_configuration() {
  $rule = rules_reaction_rule();
  $rule->label = t('Send transaction details to data layer on checkout completion');
  $rule->active = TRUE;
  $rule->event('commerce_checkout_complete')
    ->action('commerce_data_layer_send_order', array('commerce_order:select' => 'commerce_order')
    );
  $configs['commerce_data_layer'] = $rule;
  return $configs;
}