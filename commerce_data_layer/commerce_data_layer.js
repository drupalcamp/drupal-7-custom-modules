(function ($) {
Drupal.behaviors.commerce_data_layer = {
  attach: function (context, settings){
    console.log('Commerce Data Layer is Ready');


      $('.product-detail', context).once('commerce_data_product_details', function(){

              console.log('this is called only once');

              var product_detail = $('.product-detail').attr('data-product-detail');


              if(product_detail !== '') {
                  console.log("product detail is " + product_detail);

                  $.ajax(
                      {
                          'url': 'ajax-commerce/product-detail',
                          'type': 'POST',
                          'dataType': 'JSON',
                          'data': {product_id: product_detail},
                          success: function (data) {
                              console.log('product details received');
                              //   console.log(data.ecommerce.detail.products.id);
                              dataLayer.push({ 'ecommerce' : data.ecommerce });

                          },
                          error: function (XMLHttpRequest, textStatus, errorThrown) {
                              console.log('Error thrown is ' + errorThrown);

                          }
                      }
                  );

              }

              
          }
      );




 //   var product_click = $('.product-ad').getAttribute('data-product-click');

    $('.product-ad').click(function(){
        var product_click = $('.product-ad').attr('data-product-click');
        $.ajax(
            {
                'url': '/ajax-commerce/commerce_data_layer_product_click',
                'type' : 'POST',
                'dataType' : 'json',
                'data' : product_click,
                'success' : function(data){
                    console.log('product click sent');
                }
            }
        );
    });



    $('product-proceed-checkout').click(function(){
        var checkout_product = $('.express-checkout-button').attr('data-checkout-product');

        $.ajax(
            {
                'url': 'ajax-commerce/checkout',
                'type' : 'POST',
                'dataType' : 'json',
                'data' : checkout_product,
                'success' : function(data){
                    console.log('product click sent');
                }
            }
        );

        });

	
	
  }
}
}(jQuery));