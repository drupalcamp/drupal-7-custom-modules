(function ($) {

  Drupal.behaviors.simple_modals = {
      attach: function (context, settings){

          console.log('Simple Modals Init');

	    $('.playlist-chooser').change(function () {
		console.log('was changed');
		console.log(this.value);
		
		   jQuery.ajax({
		     type: 'POST',
			 url: '/simple-modals/plist-ajax/'+this.value,
			 dataType: 'json',
			 success: function(data){
			   console.log(data);
			   
			   Drupal.CTools.Modal.show('ctools-ajax-register-style');
               $('#modal-title').html('my-title');
               $('#modal-content').html('some markup here').scrollTop(0);
               Drupal.attachBehaviors();
			   
			   
			 },
		   });
		
		
		});
	  
	  
	   },

      showModal: function(title, message){
          Drupal.CTools.Modal.show('ctools-ajax-register-style');
          $('#modal-title').html(title);
          $('#modal-content').html(message).scrollTop(0);
          Drupal.attachBehaviors();
      }

	   };

}(jQuery));