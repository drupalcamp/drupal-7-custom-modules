/**
 * 
 */
function showExpertTranscript(video_id) {
    if (jQuery(".transcription-slide#" + video_id).length) {
        jQuery(".transcription-slide").remove();
    } else {
        jQuery(".transcription-slide").remove();
        var transcription = jQuery("#transcription-" + video_id);

        var row = jQuery('#video-' + video_id).parent().parent();
        row.after('<tr class="transcription-slide" id="' + video_id + '" style="display:none;"><td colspan="2">' + transcription.html() + '</td></tr>');
        jQuery(".transcription-slide").fadeIn();
    }
}
/*
 *  Hide Related Experts you may like block if there are none 
 */
Drupal.behaviors.expertsRelatedExperts = {
    attach: function(context, settings) {
        if (!jQuery('#block-apachesolr-search-mlt-002').length) {
            return;
        }

        if (!jQuery('#related_experts .expert-thumb').length) {
            jQuery('#block-apachesolr-search-mlt-002').hide();
        }

    }
};


Drupal.behaviors.expertsFacepile = {
    attach: function(context, settings) {
        if (!jQuery('#experts_facepile').length) {
            return;
        }


        var $container = jQuery('#experts_facepile');
        $container.imagesLoaded(function() {
            jQuery('.facepile_item', $container).removeClass('new');
            $container.masonry({
                itemSelector: '.facepile_item',
                isAnimated: true,
                isFitWidth: true,
                //cornerStampSelector: '.corner-stamp'
            });

            jQuery('#experts_facepile').removeClass('loading');
        });

        jQuery.autopager({
            content: ".facepile_item",
            link: '#next',
            appendTo: "#experts_facepile",
            start: function() {
                jQuery('.facepile_item', $container).removeClass('new');
                jQuery('#expertsLoader').fadeIn();
            },
            load: function(current, next) {
                var $newElems = jQuery('.new', $container);
                $newElems.imagesLoaded(function() {
                    $container.masonry('reload');
                    jQuery('#expertsLoader').hide();
                });
            }
        });

        jQuery.autopager('load');




        /*$container.imagesLoaded( function(){
        	jQuery('.facepile_item', $container).each(function() {
        		var img = jQuery('img', this);
        		if(img.length ) {
        		  var height = img.outerHeight(true)+2;
        		  jQuery(this).css('height', height);
        		}
        	});
        	
        	 $container.masonry({
        	    itemSelector : '.facepile_item',
        	    columnWidth: 52,
        	    isFitWidth: true,
        	    isAnimated: true,
        	    //cornerStampSelector: '.corner-stamp'
        	});

        	 var time = 0;
        	 var time_step = 1300;
        	 
        	 jQuery('.meet-the-experts').fadeIn(time_step);
        	 
        	 var objs = jQuery('.facepile_item', $container).get();
        	 Shuffle(objs);
        	 
        	 
        	 var step = 7;
        	 var start = 0;
        	 var end = step;
        	 var i = 0;
        	 while(i < objs.length -1) {
        	  var objs_sub = new Array();
        		 
        	  
        	   for(i = start; i < end; i++) {
        		  jQuery(objs[i]).delay(time).fadeIn(time_step);
        	   }
        	   
        	   time += time_step - 400;
        	   
        	   start = end;
        	   end = end + step;
        	   if(end > objs.length) {end = objs.length}
        	 }
        	 
        	jQuery('.facepile_item').hover(function () {
                 jQuery(this).addClass("hover");
                 jQuery('.expert_info', jQuery(this)).fadeIn();
                 
                 jQuery(this).mousemove(function(e){
                	 
                   var expertsFacepile = jQuery('#experts_facepile');
                   var offset = jQuery(this).offset();
                   xVal = e.pageX + 20 - offset.left;
                   yVal = e.pageY + 20 - offset.top;
                   
                   jQuery('.expert_info', this).css({
                     left: xVal,
                     top: yVal
                   });
                  });
        	   },
        	   function () {
        		 jQuery('.expert_info').hide();
        	     jQuery(this).removeClass("hover");
        	   }
        	 );
        	 
        	 jQuery('#experts_facepile .small').hover(function () {
                 jQuery(this).removeClass('small');
        	   },
        	   function () {
        		   jQuery(this).addClass('small');
        	   }
        	 );
        	 
        });*/

    }
};

function Shuffle(o) {
    for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};

function gung_experts_populate_more_from_block(data) {
    if (!jQuery('#more_from_expert_info').length) {
        return;
    }
    jQuery('#more_from_expert_info #experts_bio').html(data['experts_bio']);
    jQuery('#more_from_expert_info #title').html(data['name']);

}