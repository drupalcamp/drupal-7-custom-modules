INTRODUCTION
------------

Adds a new field formatter for brightcove fields which loads the brightcove player (using any specified formatter) in a ctools modal. It saves space and looks nice. Great for adding brightcove video fields to comments.

REQUIREMENTS
------------
This module requires the following modules:
 * Brightcove Media (https://www.drupal.org/project/brightcove_media)
 * Ctools (https://www.drupal.org/project/ctools)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
CONFIGURATION
-------------
 * Manage display on any content type containing a brightcove_video field. There should be a new formatter called "Brightcove Modal". Choose it and there are two configure options for the formatter - Autostart and "Default Field Formatter for Video in Modal". This defaults to "standard_video_player" but can be set to anything so it's compatible with custom formatters.
 
 