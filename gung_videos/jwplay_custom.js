var playerInstance = jwplayer("container_jwplayer");
playerInstance.setup({
    playlist: playlistGung,
    displaytitle: true,
    width: "100%",
    autostart: true,
    aspectratio: "16:9",
});

var list = '';
var html = '<div class="ListItems" style="position:inherit;"> ' +
    '  <ul class="playlist-sidebar">' +
    ' <div id="list" style="position:relative;">&nbsp;</div></ul></div>';

if (document.getElementById("playlist-results") != null) {
    list = document.getElementById("playlist-results");
    html = list.innerHTML;
    console.log("getById");
} else {
    list = html;
    console.log(list);
}

playerInstance.on('ready', function() {

    var playlist = playerInstance.getPlaylist();
    for (var index = 0; index < playlist.length; index++) {
        var playindex = index + 1;

        html += "<li class='video" + index + " playlist-item-link'><span class='dropt' title='" + playlist[index].title +
            "'><a href='javascript:playThis(" + index + ")'><img class='thumbnail'src='" +
            playlist[index].image + "'</img></br><span class='title'>" +
            playlist[index].title + "</span></br></br>" + playlist[index].desc +
            "</a></br><span style='width:500px;'></span></span></li>";
        list.innerHTML = html;
    }

});

initPlaylist(playerInstance);

function initPlaylist(player) {
    jQuery.ajax(player.getPlaylist()).then(function(data) {
        player.on('playlistItem', setActive);
    });
}

function setActive(e) {
    var id = e.item.index;
    console.log(id);
    $('li.playlist-item-link').removeClass('active');
    $('li.video' + id).addClass('active');
}

function playThis(index) {
    playerInstance.playlistItem(index);
}