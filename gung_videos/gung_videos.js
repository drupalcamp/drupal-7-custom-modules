/**
 *
 */

Drupal.behaviors.videosPageTitle = {
    attach: function(context, settings) {
        var block = jQuery('#block-gung-videos-gung-videos-of-the-day');
        if (!block.length) {
            return;
        }

        jQuery('.video-play-big', block).click(function() {
            jQuery(this).hide();
            jQuery('.video-thumb-overlay').hide();

            if (videoOfTheDay) {
                if (modVP) {
                    modVP.loadVideoByID(videoOfTheDay);
                } else {
                    //Its being initialized so save in global variable
                    startingVideo = videoOfTheDay;
                }
            }
        });



    }
}


Drupal.behaviors.videoActionButtons = {
    attach: function(context, settings) {
        if (!Drupal.settings.authenticated_user) {
            jQuery('#discuss_button a').unbind('click');
        }

    }
};