(function($) {



    Drupal.behaviors.gungcustomvideos = {
        attach: function(context, settings) {
            //code starts

            $(document).ready(function() {
                var expertVids = [];


                if ($("body").hasClass("node-type-expert")) {

                    $('.expert-play-vid').each(function(i) {
                        //  console.log($(this).data('bc-video-id'));
                        expertVids.push($(this).data('bc-video-id'));
                    });
                    //console.log(expertVids[0]);
                    //console.log('node expert page');
                    //console.log('jumpVid is'+jumpVid);

                    if (!$("body").hasClass("gung-mobile2")) {
                        setVidList(expertVids);
                        setVidqueue(expertVids);


                        setTimeout(function() {
                            if (jumpVid !== '') {
                                advanceVidqueue(jumpVid);
                                jumpVid = '';
                            } else {
                                queueVidstoPlay();
                            }
                        }, 3000);


                    }
                    //setTimeout(function(){
                    //	modVP.loadVideoByID(expertVidNids[0]);
                    //    }, 3000);
                    // uses timeout function for now, should really listen for when player is initialized.

                } else if (($("body").hasClass("page-taxonomy-term") || $("body").hasClass("node-type-playlist")) && !$("body").hasClass("gung-mobile2")) {
                    //	console.log('taxonomy video page passed');
                    var expertVids = [];
                    $('a.expert-play-vid').each(function(i) {
                        //  console.log($(this).data('bc-video-id'));
                        expertVids.push($(this).data('bc-video-id'));
                    });
                    //	console.log(expertVids);
                    //  console.log('jumpVid is'+jumpVid);
                    setVidList(expertVids);
                    setVidqueue(expertVids);



                    setTimeout(function() {
                        if (jumpVid !== '') {
                            //currentVid = jumpVid;
                            advanceVidqueue(jumpVid);
                            jumpVid = '';
                        } else {
                            queueVidstoPlay();
                        }
                    }, 3000);
                }
            });

            $('.expert-play-vid').click(function() {
                //console.log($(this).('bc-video-id'));
                //modVP.loadVideoByID($(this).data('bc-video-id'));
                currentVid = $(this).data('bc-video-id');
                advanceVidqueue($(this).data('bc-video-id'));
            });

            $('.video-search-result-video').click(function() {
                //console.log($(this));

                //swapExpertBio($(this).find(".teaser-bio-data").html());
                advanceVidqueue($(this).find(".expert-play-vid").data('bc-video-id'));

            });

            //code ends
        }
    };
})(jQuery);