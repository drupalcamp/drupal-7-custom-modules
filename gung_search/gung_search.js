// jQuery(document).ready(function ($) {
    // $('#block-search-form').delegate('#search-block-form', 'submit', function (event) {
        /*event.preventDefault();
        var keys = $('#edit-search-block-form--2').val();
        get_search_block(keys);*/
    // });
// });


Drupal.behaviors.solrSearchListsContentBoard= {
  attach: function(context, settings) {
	var $container = jQuery('#videos-search-results');
    if(!$container.length) {
      return;
    }

    $container.imagesLoaded( function(){
    	jQuery('.video_item', $container).removeClass('new');
    //   $container.masonry({
	  //   itemSelector : '.video_item',
	  //   isAnimated: false,
	  //   isFitWidth: false,
	  //   //cornerStampSelector: '.corner-stamp'
	  // });
    });

    jQuery.autopager({content: ".video_item", link: '#next', appendTo: "#videos-search-results",
    	start: function() {
    		jQuery('.video_item', $container).removeClass('new');
    		jQuery('#solrSearchListsLoader').fadeIn();
    	},
		 load: function(current, next){
			 var $newElems = jQuery('.new', $container).css({ opacity: 0 });
			 $newElems.imagesLoaded( function(){
		       $newElems.css({ opacity: 1 });
			   //$container.masonry( 'appended', $newElems, true );
			 });
			 jQuery('#solrSearchListsLoader').hide();
		 }
	});



  }
}
