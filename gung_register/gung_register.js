/**
 *
 */

jQuery(document).ready(function() {
    /*window.fbAsyncInit = function() {
        FB.init(Drupal.settings.fb.fb_init_settings);

       //showLoader(true);

       // run once with current status and whenever the status changes
       FB.getLoginStatus(updateButton);
       FB.Event.subscribe('auth.statusChange', updateButton);
    };*/
    jQuery(document).bind('fb_session_change', gungRegisterSessionChangeHandler);
});

var sessionHandlerTriggers = false;
//JQuery pseudo-event handler.
gungRegisterSessionChangeHandler = function(context, status) {
    sessionHandlerTriggers = true;
};

Drupal.behaviors.facebookConnect = {
    attach: function(context, settings) {
        if (!jQuery('#modalContent .fb-auth').length) {
            return;
        }

        FB_JS.getLoginStatus(updateButton, true);
        FB.Event.subscribe('auth.statusChange', updateButton);

    }
}

function gung_register_post_facebook() {
    if (jQuery('#modalContent').length) {
        jQuery('#step_2_button a').trigger('click');
    }
}

function updateButton(response) {
    FB_JS.fbAsyncInitFinal(response);

    button = document.getElementById('fb-auth');
    userInfo = document.getElementById('user-info');

    /*  if (response.authResponse) {
    	//user is already logged in and connected
    	  showLoader(true);
      } else {*/
    //user is not connected to your app or logged out
    jQuery('.fb-auth').once('facebook-connected-proccessed').click(function() {
        showLoader(true);
        FB.login(function(response) {
            if (response.authResponse) {
                FB.api('/me', function(info) {
                    if (!sessionHandlerTriggers) {

                        var status = {
                            'changed': true,
                            'fbu': FB.getUserID(),
                            'response': response
                        };

                        jQuery.event.trigger('fb_session_change', status);
                    }
                });
            } else {
                //user cancelled login or did not grant authorization
                showLoader(false);
            }
        }, { scope: 'email,status_update,publish_actions,user_about_me' });

    });
    //}
}

Drupal.behaviors.registerHideVideo = {
    attach: function(context, settings) {
        jQuery('.close').click(function() {
            // jQuery('#video-player-intro-video').hide();
        });
    }
}

function login(response, info) {
    if (response.authResponse) {
        console.log('FB Log: ' + response.authResponse)
    }
}

function showLoader(status) {
    if (status) {
        jQuery('#user-login #form_fields #form_content').hide();
        jQuery('.replace_box').hide();
        jQuery('.facebook_connecting').fadeIn();
    } else {
        jQuery('#user-login #form_fields #form_content').fadeIn();
        jQuery('.replace_box ').fadeIn();
        jQuery('.facebook_connecting').hide();
    }
}


Drupal.behaviors.registerHideVideo = {
    attach: function(context, settings) {
        jQuery('.close').click(function() {
            // jQuery('#video-player-intro-video').hide();
        });
    }
}

Drupal.behaviors.showChildren = {
    attach: function(context, settings) {
        if (!jQuery('.register-step2').length || jQuery('.gungmobile').length) {
            return;
        }

        var child = jQuery(".register_children");
        var parent = jQuery('.form-item-field-is-parent-und .form-checkbox');
        var expecting = jQuery('.form-item-field-is-expecting-und .form-checkbox');

        var firstDate = jQuery('#gender-' + 0).parent().parent().parent();
        var secondDate = jQuery('#gender-' + 1).parent().parent().parent();

        //show/hide Children
        if (parent.attr("checked")) {
            child.show();
            aboutFamilyToggle(true);
            jQuery('#how-many').show();
            jQuery("#how-many input").trigger('keyup');
            jQuery("#children-form tr").hide();
        } else {
            if (!expecting.attr("checked")) {
                child.hide();
                jQuery('#how-many').hide();
                aboutFamilyToggle(false);
            }
        }

        if (expecting.attr("checked")) {
            child.show();
            aboutFamilyToggle(true);
            jQuery('#due-date').show();
            firstDate.show();
        } else {
            if (!parent.attr("checked")) {
                child.hide();
                aboutFamilyToggle(false);
            }
            jQuery('#due-date').hide();
            firstDate.hide();
            jQuery('#age-' + 0 + ' input').val('');
        }

        parent.click(function() {
            if (jQuery(this).attr("checked") || expecting.attr("checked")) {
                child.show();
                jQuery("#children-form tr").hide();
                aboutFamilyToggle(true);
            } else {
                child.hide();
                aboutFamilyToggle(false);
            }

            if (jQuery(this).attr("checked")) {
                jQuery('#how-many').show();
            } else {
                jQuery('#how-many').hide();
            }
        });

        expecting.click(function() {
            if (jQuery(this).attr("checked") || parent.attr("checked")) {
                child.show();
                aboutFamilyToggle(true);
            } else {
                child.hide();
                aboutFamilyToggle(false);
            }

            if (jQuery(this).attr("checked")) {
                jQuery('#due-date').show();
                firstDate.show();
            } else {
                jQuery('#due-date').hide();
                firstDate.hide();
                jQuery('#age-' + 0 + ' input').val('');
            }
        });

        jQuery("#how-many input").keyup(function() {
            var num_children = jQuery(this).val();
            var valid = jQuery('#how-many #valid')
            jQuery('#children-form tr').hide();

            if (expecting.attr("checked")) {
                jQuery('#due-date').show();
                firstDate.show();
            }

            if (!isNumber(num_children)) {
                valid.html('<span style="color: red; font-style: italic; font-size: 10px;">Not a Number</span>');
            } else if (num_children == 0) {
                valid.html('<span style="color: red; font-style: italic; font-size: 10px;">Oops, parents can\'t have 0 children.');
            } else if (num_children == 1) {
                valid.html('Nice! What is his/her birthday?');
                secondDate.show();
            } else {
                valid.html('Nice! What are their birthdays?');
                for (var i = 1; i <= num_children; i++) {
                    jQuery('#gender-' + i).parent().parent().parent().show();
                }
            }
        });
    }

    //Add Event
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}


function aboutFamilyToggle(hide) {
    var family = jQuery("#register_about_family");

    var imagePath = 'url("/sites/all/themes/gungwang/images/little_family.png") no-repeat scroll right bottom transparent';
    if (!hide && !jQuery('body').hasClass('gungmobile')) {
        family.css('background', imagePath);
    } else {
        family.css('background', 'none');
    }
}


function openIntroVideoPlayer() {
    //openVideoPlayer();
    var videoPlayerContent = jQuery('#video-player-intro-video');
    videoPlayerContent.attr('style', 'background-color: white; display: block; position: absolute; top:  25px; z-index: 1002;');

    var modalContent = jQuery('#modalContent').hide();
    jQuery('#videos_player_close', videoPlayerContent).show();

    var left = modalContent.css('left');
    videoPlayerContent.center();
    jQuery(window).bind('resize', introVideoPlayerResize);

    if (modIntroVP) {
        modIntroVP.loadVideoByID(introVideo);
        modIntroVP.play();
    } else {
        //Its being initialized so save in global variable
        startingVideo = introVideo;
    }
}

function closeIntroVideo() {
    var videoPlayerContent = jQuery('#video-player-intro-video');
    jQuery('#modalContent').show();
    videoPlayerContent.hide();
    if (modIntroVP) {
        modIntroVP.pause();
    }
}

function introVideoPlayerResize() {
    var videoPlayerContent = jQuery('#video-player-intro-video');
    if (videoPlayerContent.is(":visible")) {
        videoPlayerContent.center();
        jQuery('#modalContent').hide();
    }
}

/*
 Gung: 10/12/2016
*/
jQuery("#login_here").click(function() {
    jQuery("#register_box_signup").hide();
    jQuery("#register_box_login").show("fast");
});

jQuery("#create_account_here").click(function() {
    jQuery("#register_box_login").hide();
    jQuery("#register_box_signup").show("fast");
});