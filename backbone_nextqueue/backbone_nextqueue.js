(function($) {
    Drupal.behaviors.backbone_nextqueue = {


        anotherFunction: function() {
            // console.log('anotherFunction');
        },


        attach: function(context, settings) {
            // console.log('Module JS is working');


            function myTemplateLoaded(experienceID) {
                player = brightcove.api.getExperience(experienceID);
                modVP = player.getModule(brightcove.api.modules.APIModules.VIDEO_PLAYER);
                modExp = player.getModule(brightcove.api.modules.APIModules.EXPERIENCE);
            }


            function onTemplateReady(evt) {
                modVP.addEventListener(brightcove.api.events.MediaEvent.COMPLETE, onExpertComplete);
            }

            function onExpertComplete(evt) {
                // console.log('Media is complete called in backbone_nextqueue');

                if (jQuery('.playlist-next-backbone').length) {
                    jQuery('.playlist-next-backbone').trigger("click");
                }
                //// console.log('expertVidNids');
                else if (jQuery('.view-videos-by-expert a.more-expert-vids').hasClass('playnext') || jQuery('#block-apachesolr-search-mlt-001 a').hasClass('playnext')) {
                    // console.log('refresh is about to be called');
                    refreshvid();
                } else {
                    // console.log(evt);
                    // console.log('queueVidstoPlay is about to be called');
                    queueVidstoPlay();
                }

            }

            window.gung = {
                Models: {},
                Collections: {},
                Views: {},
                Router: {},

                start: function() {
                    var videos = new gung.Collections.Videos();
                    videos.fetch({
                        error: function() {
                            // console.log(arguments); 
                        }
                    });

                    // var existingid = GetURLParameter('id');

                    var nextvideo = videos.first();
                    if (nextvideo) {
                        nextvideo = nextvideo.nextElement();
                        // console.log(nextvideo);
                    }

                    var playlistView = new gung.Views.Playlist({ collection: videos });

                    if (jQuery("body").hasClass("node-type-playlist")) {
                        GungRouter = new gung.Router;
                        var existingid = GetURLParameter('id');
                        var existinghash = window.location.hash;

                        // console.log(existingid);
                        if (existingid && !window.location.hash) {
                            var newUrl = window.location.pathname + window.location.search + '#playvid/' + existingid;
                            // console.log('update hash');
                            addPlayer();
                            //  window.location.assign(newUrl);
                        }

                        if (!existingid && !window.location.hash) {
                            window.location.href = jQuery('.video-search-result-video a.expert-play-vid').attr('href');
                            // console.log('hash 2');
                        }

                        GungRouter.on('route:play', function(url) {

                            url = '/' + url;
                            var link = url;
                            var singlevid = videos.findWhere({ 'url': link });
                            var nid = singlevid.get("id");
                            var newUrl = window.location.pathname + '#playvid/' + nid;
                            window.location.assign(newUrl);

                            if (jQuery("body").hasClass("not-logged-in")) {
                                //    window.location.reload(true);
                                // console.log(newUrl);
                                window.location.reload();
                            }


                        });


                        GungRouter.on('route:playvid', function(id) {
                            var vid = videos.get(id);
                            if (vid) {
                                var bcvid = vid.get('bcvid');
                                // console.log('bvid');
                            } else {
                                // console.log('rebuilding');
                                playlistView.reBuildJump(id);
                            }

                            swapExpertBio(jQuery('.teaser-bio-data[data-bc-video-id="' + bcvid + '"]').html(), jQuery('.teaser-bio-data[data-bc-video-id="' + bcvid + '"] .more-expert-vids-link').html());
                            var transcript = jQuery('.fulltranscription[data-bc-video-id="' + bcvid + '"]').html();
                            //  // console.log('transcript is '+transcript);
                            jQuery('#transcription_text').html(transcript);
                            jQuery('#active_video_nid').text('' + id);
                            jQuery('.expert-play-vid[data-bc-video-id="' + bcvid + '"]').parent('.video-search-result-video').addClass('active');
                            jQuery('.nowPlayingMask').hide();
                            jQuery('.expert-play-vid[data-bc-video-id="' + bcvid + '"]').siblings('.nowPlayingMask').show();

                            // console.log(url);
                            addPlayer();


                            setTimeout(function() {
                                // console.log('main player about to show');
                                jQuery('#video_player_inner_wrapper').show();
                            }, 3000);


                            setTimeout(function() {

                                // console.log('Adding end function');
                                videojs("main_player").ready(function() {
                                    // console.log('player is ready');
                                    myPlayer = this;
                                    // console.log(myPlayer);
                                    myPlayer.on("ended", function() {
                                        // console.log('video has ended');
                                        onMediaComplete();
                                        onExpertComplete();
                                    });
                                });
                            }, 10000);

                        });

                        //Backbone.history.start();
                        //if (!Backbone.history.start()) router.navigate('404', {trigger:true});
                        try {
                            Backbone.history.start();
                        } catch (err) {
                            console.log('backbone.htistory' + err.message);
                        }
                    }


                }
            };

            gung.Views.Playlist = Backbone.View.extend({
                el: $('body'),

                events: {
                    'click a.sample-link': 'changeVideo',
                    'click a.more-expert-vids': 'changeVideo',
                    'click a.previous-backbone': 'goPrev',
                    'click a.next-backbone': 'goNext',
                    'click a.playlist-next-backbone': 'goNextPlaylist',
                    'click a.playlist-previous-backbone': 'goPrevPlaylist',
                    'click a.rebuild-backbone': 'reBuild',
                    //    'click a.expert-play-vid' : 'playlistQueue'
                },


                initialize: function() {
                    // console.log('playlist view init');

                    var currentpath = window.location.pathname;
                    // console.log(currentpath);
                    // console.log(this.collection);
                    // this.gotovideo(this.collection, '/teenager/social-life/peer-pressure/helping-teens-unmet-desire-fit?qt-more_videos=1#qt-more_videos');

                    if (this.collection.length == 0) {
                        this.buildexpertvideolist(this.collection);
                    }

                },

                playlistQueue: function(e) {
                    // console.log('playlistQueue called');
                    //  e.preventDefault();
                    //// console.log()


                },

                reBuild: function(e) {
                    if (e) {
                        e.preventDefault();
                    }
                    // console.log('Playlist is being rebuilt');
                    this.buildexpertvideolist(this.collection);
                },

                reBuildJump: function(id) {
                    this.buildexpertvideolist(this.collection, id);
                },

                goNextPlaylist: function(e) {
                    e.preventDefault();
                    // console.log('goNextPlaylist called');

                    var unwatched = this.collection.where({ watched: false });

                    if (unwatched == '') {
                        this.reBuild(e);
                    }

                    nextvid = unwatched[0];
                    var currentURL = window.location.pathname + window.location.search + window.location.hash;
                    // console.log('Current URL is '+currentURL);
                    var currentvideo = this.collection.findWhere({ 'directurl': currentURL });
                    // console.log(currentvideo);
                    var singlevideo = currentvideo.nextElement();

                    if (singlevideo) {
                        singlevideo.set({ watched: true });
                        window.location.href = singlevideo.get('directurl');
                    } else {
                        this.reBuild(e);

                    }

                },

                goPrevPlaylist: function(e) {
                    e.preventDefault();
                    // console.log('goPrevPlaylist called');

                },

                goNext: function(e) {
                    e.preventDefault();
                    // console.log('goNext called');

                    var unwatched = this.collection.where({ watched: false });

                    if (unwatched == '') {
                        this.reBuild(e);
                    }

                    //        // console.log('unwatched is');
                    //        // console.log(unwatched);
                    //        // console.log(unwatched[0]);
                    //     var nextvid = this.collection.first();
                    nextvid = unwatched[0];
                    // console.log('Collection is');
                    // console.log(this.collection);
                    //  nextvid.set("watched", "true");
                    if (nextvid) {


                        nextvid.set({ watched: true });
                        // console.log(nextvid);

                        if (window.location.href == nextvid.get('url')) {
                            // console.log('already true');
                            // console.log(nextvid.nextElement());

                            nextvid = unwatched[1];
                            //var unwatched = this.collection.where({watched: false});
                            //   nextvid = this.collection.first();
                            // console.log(nextvid);
                            // console.log('Collection is');


                            var link = nextvid.get('url');
                            //nextvid.destroy();
                            nextvid.set({ watched: true });

                            // console.log(this.collection);
                            // window.location.href = nextvid.set({watched: true}).get('url');

                            if (jQuery("body").hasClass("node-type-playlist")) {
                                // console.log('link is');

                                var newUrl = window.location.pathname + '#play' + link;
                                window.location.href = newUrl;

                            } else {
                                window.location.href = link;
                            }
                        } else {
                            link = nextvid.get('url');
                            nextvid.set({ watched: true });
                            //nextvid.destroy();



                            if (jQuery("body").hasClass("node-type-playlist")) {
                                // console.log('link is');
                                // console.log(link);
                                //    var newUrl = window.location.pathname+'#play'+link;
                                //     window.location.href = newUrl;
                            } else {
                                window.location.href = link;
                            }

                        }
                    } else {
                        this.reBuild(e);
                    }

                },

                goPrev: function(e) {
                    e.preventDefault();
                    // console.log('goPrev called');
                    var nextvid = this.collection.last();
                    window.location.href = nextvid.get('url');
                },

                buildexpertvideolist: function(collection, id) {
                    // console.log('this was called');
                    collection.reset();


                    _.chain(collection.models).clone().each(function(model) {
                        // console.log('deleting model ' + model.id);
                        model.destroy();
                    });

                    if (jQuery("body").hasClass("node-type-playlist")) {
                        // console.log('on a playlist');

                        jQuery('.video-search-result-video').each(function(i) {
                            var link = jQuery("a.expert-play-vid", this).attr('data-direct-path');
                            var direct = jQuery("a.expert-play-vid", this).attr('href');
                            var title = jQuery(".title", this).text();
                            var nid = this.id;
                            var bcvid = jQuery(this).attr('data-bc-video-id');
                            nid = nid.substring(6);
                            // console.log('nid is'+nid);
                            // console.log('link is'+link);
                            // console.log('title is'+title);
                            // console.log('bcvid is'+bcvid);
                            // console.log('direct is'+direct);
                            collection.create({
                                'id': nid,
                                'url': link,
                                'directurl': direct,
                                'title': title,
                                'bcvid': bcvid,
                                'watched': false
                            });

                        });

                        if (jQuery("#main-player-info").data("mp-video-id") == '') {
                            // console.log('no vid is playing');

                        }
                    } else {
                        jQuery('a.more-expert-vids').each(function(i) {
                            var link = jQuery(this).attr('href');
                            var title = jQuery(this).text();

                            // console.log('link is'+link);
                            // console.log('title is'+title);

                            collection.create({
                                'url': link,
                                'title': title
                            });

                        });
                    }


                    //       // console.log(this.collection);
                    if (id) {
                        var initialvid = collection.get(id);
                        window.location.href = initialvid.get('directurl');
                        window.location.reload();
                    } else {
                        var initialvid = collection.first();
                        // console.log(initialvid);
                        window.location.href = initialvid.get('directurl');

                    }

                },

                changeVideo: function(e) {
                    // // console.log('changeVideo called');
                    e.preventDefault();

                    var href = $(e.currentTarget).attr("href");
                    // console.log(href);
                    this.gotovideo(this.collection, href);
                },

                gotovideo: function(collection, link) {
                    if (link.length > 0) {
                        //      // console.log('link length exists');
                        var singlevideo = collection.findWhere({ url: link });

                        if (singlevideo) {
                            collection.each(
                                function(model) {
                                    model.unset({ watched: true });
                                }
                            );

                            singlevideo.set({ watched: true });
                            // console.log(singlevideo);
                            window.location.href = singlevideo.get('url');

                        } else {
                            collection.reset();
                            window.location.href = link;

                        }
                        // clear previous watched - set watched attribute to singlevideo and redirect to url
                    } else {
                        //      // console.log('go to next unwatched video');
                        var singlevideo = collection.findWhere({ watched: true })
                        singlevideo = singlevideo.nextElement();
                        if (singlevideo) {
                            singlevideo.set({ watched: true });
                            window.location.href = singlevideo.get('url');
                        }
                        //find last watchd video store id, unset watched - go to the next id, set watched a redirect
                    }
                },
                buildrelatedvideolist: function() {

                },
            });

            gung.Models.Video = Backbone.Model.extend({
                default: {
                    title: '',
                    url: null,
                    watched: false,
                    videoid: null,
                    bcvid: null
                },

                nextElement: function() {
                    var index = this.collection.indexOf(this);
                    if ((index + 1) === this.collection.length) {
                        //It's the last model in the collection so return null
                        return null;
                    }
                    return this.collection.at(index + 1);
                },

                previousElement: function() {
                    var index = this.collection.indexOf(this);
                    if (index === 0) {
                        //It's the first element in the collection so return null
                        return null;
                    }
                    return this.collection.at(index - 1);
                }


            });

            gung.Collections.Videos = Backbone.Collection.extend({
                model: gung.Models.Video,

                initialize: function() {
                    // console.log('Videos Setup');
                },

                localStorage: new Backbone.LocalStorage("gung-backbone")

            });

            gung.Router = Backbone.Router.extend({
                routes: {
                    '': 'main',
                    'playnext': 'playNext',
                    'playprev': 'playPrev',
                    'play/*url': 'play',
                    'playvid/:id': 'playvid'
                },

                playNext: function() {
                    // console.log('playNext next was called');
                }
            });

            // console.log('this works');
            //var gung = {}; // create namespace for our app

            gung.Playlist = Backbone.View.extend({
                el: $('body'),
                //events for playlist
                events: {
                    'click .next-backbone': 'playNext',
                    'click .previous-backbone': 'playPrev',
                },

                initialize: function() {
                    // console.log('Playlist initialized');
                    var expertVids = [];
                    jQuery('a.more-expert-vids').each(function(i) {
                        var title = jQuery(this).attr('href');
                        var titletext = jQuery(this).text();
                        // console.log(titletext);
                        // console.log(jQuery(this).data('bc_video_id'));
                        // console.log(jQuery(this).data('bc-video-id'));
                        expertVids.push(jQuery(this).data('bc-video-id'));
                    });
                    // console.log(expertVids);
                },

                playNext: function(options) {
                    // console.log('play next called');
                    // console.log(options);
                },
                playPrev: function(options) {
                    // console.log('playPrev was called');
                },
                goFirst: function(options) {
                    // console.log('go first');
                },
                goLast: function(options) {
                    // console.log('go last');
                },
                playVid: function(options) {
                    // console.log('PlayVid');
                },
                rebuildQueue: function(options) {
                    // console.log('rebuildQueue');
                },
                clearQueue: function(options) {
                    // console.log('clearQueue');
                },

            });

            gung.Video = Backbone.Model.extend({
                defaults: {
                    url: null,
                    nid: null,
                    uid: null,
                    title: '',
                    videoid: null,
                    watched: false
                }
            });

            gung.VideoList = Backbone.Collection.extend({
                model: gung.Video,

                initialize: function() {
                    // console.log('VideoList setup');
                },
                remove: function() {
                    // console.log('Remove a model from VideoList was called');
                    // console.log(this.model);
                }

            });

            gung.start();

        }
    };
}(jQuery));