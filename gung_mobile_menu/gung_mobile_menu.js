(function($) {

    Drupal.behaviors.gungmobilemenu = {
        attach: function(context, settings) {
            console.log('gungmobilemenu is ready');

            jQuery('#block-menu-menu-gung-mobile2-menu').once('responsive-menus-codrops-multi-menu', function() {

                jQuery(this)
                    .attr('class', 'dl-menuwrapper')
                    .attr('id', 'dl-menu')
                    .css('z-index', '999');
                // Find the parent ul.
                var jQueryparent_ul = jQuery(this).find('ul:not(.contextual-links)').first();
                jQueryparent_ul
                    .attr('class', 'dl-menu')
                    .attr('id', 'rm-dl-menu')
                    .find('li').removeAttr('id').removeAttr('class')
                    .find('a').removeAttr('id').removeAttr('class');
                // Add submenu classes.
                jQueryparent_ul.find('ul').each(function(subIndex, subMenu) {
                    jQuery(this).removeAttr('id').attr('class', 'dl-submenu');
                    jQuerysubnav_link = jQuery(this).parent('li').find('a').first()[0].outerHTML;
                    jQuery(this).prepend('<li>' + jQuerysubnav_link + '</li>');
                });
                // Call the ResponsiveMultiLevelMenu dlmenu function.
                jQuery(this).dlmenu({
                    animationClasses: {
                        classin: 'dl-animate-in-1',
                        classout: 'dl-animate-out-1'
                    }
                });


            });


        }
    }



}(jQuery));