<?php

/**
 * Returns image field formatter.
 *
 * @param $variables
 *   An associative array containing:
 *   - items: An array of image data.
 *   - list-style: An array of image data.
 *
 * @ingroup themeable
 */

function theme_gung_facebook_tabs_list($variables) {
	global $theme;
	// drupal_add_js(drupal_get_path('module', 'gung_facebook_tabs')  .'/gung_facebook_tabs.js');
	drupal_add_css(drupal_get_path('module', 'gung_facebook_tabs') .'/gung_facebook_tabs.css');

	$playlist_video_ids = array();
	$thumb_items = array();

	$playlist = $variables['playlist'];
	$items = $variables['items'];
	foreach($items as $item) {
		$video_node = node_load($item['nid']);
			$video_node->ajax_vid = true;

			if(arg(0) == 'user' || arg(0) == 'forum') {
			  $video_node->ajax_vid = false;
			}

			//If AJAX vid, than use actual path, for not, jump to Video Player page and play there
			if($video_node->ajax_vid || !isset($playlist)) {
				$path = url('node/' . $video_node->nid);
			} else {

				$path = url('node/' . $playlist->nid, array('query' => array( 'id' => $video_node->nid) ));

			}
			if(isset($video_node->nid)) {
				$thumb_items[] = ($theme == 'gungmobile' ? '<div class="twelve mobile-twelve columns playlist-item">'. render(node_view($video_node, 'teaser')). '</div>' : render(node_view($video_node, 'teaser')));
			}

	}



	//jCarousel Theme
	$options = array (
			'skin' => 'gungwang',
			'scroll' => 5,
			'animation' => 'slow',
			'easing' => 'swing',
			'wrap' => null,
			'initCallback' => 'carousel_initCallback',
	);

	$thumb_view = theme('jcarousel', array('items' => $thumb_items, 'options' => $options, ));

	$output = '<h2 class="playlist-results-title block-title" >' . $playlist->title . '</h2>';
	if($theme == 'gungmobile') {
		// dpm(get_defined_vars(), 'defined vars');
		global $base_url;
		$title = $variables['playlist']->title;
		$output .= '<div class="breadcrumb">'. l(t('Playlists'), $base_url. '/playlists'). '&nbsp > &nbsp'. $title. '</div><div id="playlist-results" class="videos-thumb-view">' . implode($thumb_items) . '</div>';
	}
	else {
	$output	.= '<div style="margin-left: 10px; float: left; margin-top: 2px;">' . gung_global_short_add_this_button() . '</div>';
  	$output .= '<div id="playlist-results" class="videos-thumb-view">' . $thumb_view . '</div>';
  }

  return $output;
}
