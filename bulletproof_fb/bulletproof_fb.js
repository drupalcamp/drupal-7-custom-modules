(function ($) {


  Drupal.behaviors.bulletproof_fb = {
    attach: function (context, settings){
      $('body').once(function(){
        Drupal.behaviors.bulletproof_fb.init(context);
      });

      $('.bpfb-connect-dl').click(function(){
        //console.log('Facebook authentication is clicked');
        $('#user-login #form_content').fadeOut();
        $('#register_form .replace_box').fadeOut();
        $('.facebook_connecting').fadeIn();

        FB.login(function(response) {
          Drupal.behaviors.bulletproof_fb.loginStatusChangeCallback(response);
          // handle the response
        }, {scope: 'public_profile,email'});
        // Drupal.behaviors.bulletproof_fb.checkLoginState();
      });

    },

    init: function(context){
      //   console.log('bulletproof_fb is init');
      //   console.log(context);

      var jssource = '';
      if(Drupal.settings.bulletproof_fb.mode == "dev"){
        var jssource = "//connect.facebook.net/en_US/sdk/debug.js";
      }
      else{
        jssource = "//connect.facebook.net/en_US/sdk.js";
      }

      window.fbAsyncInit = function() {
        FB.init({
          appId      : Drupal.settings.bulletproof_fb.appid,
          cookie     : true,
          xfbml      : true,
          version    : 'v2.8',
          status     : 'true'
        });
      };

      (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = jssource;
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));



      var cookie_value = jQuery.cookie("DRUPAL_UID");
      if(cookie_value != 0)
      {
        console.log('authenticated');
        // Drupal user is authenticated...do some code...
      }
      else {
        // Drupal user anonymous user...do some code...
        console.log('anonymous');



      }
    },

    connectExisting: function(){

    },

    login: function(){

      console.log('access token is: '+FB.getAuthResponse().accessToken);
      document.cookie = 'fb_token=' + FB.getAuthResponse().accessToken;
      this.PropagateSS(FB.getAuthResponse().accessToken);
    },

    logout: function(){
      document.cookie = 'fb_token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
      this.PropagateSS();
    },


    PropagateSS: function(accesstoken){

      console.log('PropagateSS called');



      var url = '/me?fields=name,email';
      FB.api(url, function(response) {
        console.log('Successful login for: ' + response.name);
        var urlPar = FB.getAuthResponse().accessToken + '?id=' +
        response.id + '&name=' + response.name + '&email='+  response.email ;

        $.ajax({
          url: '/ajax/bulletproof-fb/connect/' + urlPar,
          dataType: 'json',
          success: function (data, status) {
            console.log("data=");
            console.log( data);
            window.location.reload();
          },
          error: function(data, e1, e2) {
            console.log("error: ");
            console.log( data);
            console.log( e1);
            console.log( e2);
            //window.location.reload();
          }
        });
      });


      /*
      FB.api('/me', function (response) {
      var email = response.email;
      var name = response.name;
      // used in my mvc3 controller for //AuthenticationFormsAuthentication.SetAuthCookie(email, true);
        });
        console.log('Successful login for: ' + FB.getAuthResponse().name);
        var urlPar = FB.getAuthResponse().accessToken +
        '?id=' +FB.getAuthResponse().id +
        '&name=' +FB.getAuthResponse().name +
        '&email='+FB.getAuthResponse().email ;

        $.ajax({
        // url: '/ajax/bulletproof-fb/connect/' + FB.getAuthResponse().accessToken,
        url: '/ajax/bulletproof-fb/connect/' + urlPar,
        dataType: 'json',
        success: function (data) {
        console.log("data=");
        console.log(data);
        window.location.reload();
        // do whatever you want.
        // data.status: bool, true => login
        // [data.message: string]
        // [data.more: string]
      }
    });

    */

  },


  loginStatusChangeCallback: function(response){
    console.log('loginStatusChangeCallback');
    console.log(response);

    if (response.status === 'connected') {
      //login();
      //check if already logged in as authenticated, if not login
      //  console.log('connected');
      Drupal.behaviors.bulletproof_fb.login();


    } else {
      //   logout();
      //    console.log('disconnected');
      Drupal.behaviors.bulletproof_fb.logout();

    }
  },

/*
  checkLoginState: function(){
    //console.log('checkLoginStage was called');

    FB.getLoginStatus(function(response){
      Drupal.behaviors.bulletproof_fb.loginStatusChangeCallback(response);
    });
    //  setTimeout("FB.getLoginStatus(function(response){console.log('got login status'); console.log(response); Drupal.behaviors.bulletproof_fb.loginStatusChangeCallback(response);})", 4000);
  },

*/

};
//  console.log('bulletproof_fb.js is ready');

}(jQuery));
