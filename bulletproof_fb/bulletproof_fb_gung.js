
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      propagateSS(response);
    } else {
      // The person is not logged into your app or we are unable to tell.
      // document.getElementById('status').innerHTML = 'Please log ' +
      //   'into this app.';
      console.log('bulletprof_fb: no connection!');
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  // $('.bpfb-connect-dl').click(function(){
  //   console.log('bpfb-connect-dl');
  //   FB.getLoginStatus(function(response) {
  //     statusChangeCallback(response);
  //   });
  // });

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '329129747188249',
      cookie     : true,  // enable cookies to allow the server to access
      // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });

    // Now that we've initialized the JavaScript SDK, we call
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

    // FB.getLoginStatus(function(response) {
    //   console.log('FB.getLoginStatus');
    //   statusChangeCallback(response);
    // });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));


function propagateSS(){

  console.log('PropagateSS called');

  var url = '/me?fields=name,email';
  FB.api(url, function(response) {
    console.log('Successful login for: ' + response.name);
    var urlPar = FB.getAuthResponse().accessToken + '?id=' +
    response.id + '&name=' + response.name + '&email='+  response.email ;

    jQuery.ajax({
      url: '/ajax/bulletproof-fb/connect/' + urlPar,
      dataType: 'json',
      success: function (data, status) {
        console.log("data=");
        console.log( data);
        //window.location.reload();
      },
      error: function(data, e1, e2) {
        console.log("error: ");
        console.log( data);
        console.log( e1);
        console.log( e2);
        //window.location.reload();
      }
    });
  });

}


// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function testAPI() {
  console.log('Welcome!  Fetching your information.... ');
 var url = '/me?fields=name,email';
  FB.api(url, function(response) {
    console.log('Successful login for: ' + response.name);
    document.getElementById('status').innerHTML =
      'Thanks for logging in, '  + response.id + ' & ' + response.name + '& email:'  + response.email;
   console.log(response );
  });
}

/*
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
*/
