<?php

function gung_check_users_default_views(){

$view = new view;
$view->name = 'are_they_registered';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'users';
$view->human_name = 'Are they registered';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Are they registered';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'access user profiles';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'uid' => 'uid',
  'name' => 'name',
  'mail' => 'mail',
  'login' => 'login',
  'created' => 'created',
  'status' => 'status',
  'field_direct_referer' => 'field_direct_referer',
  'field_refered_by' => 'field_refered_by',
  'php' => 'php',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'uid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'mail' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'login' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_direct_referer' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_refered_by' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'php' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['override'] = 1;
$handler->display->display_options['style_options']['sticky'] = 0;
$handler->display->display_options['style_options']['empty_table'] = 0;
/* Header: Global: PHP */
$handler->display->display_options['header']['php']['id'] = 'php';
$handler->display->display_options['header']['php']['table'] = 'views';
$handler->display->display_options['header']['php']['field'] = 'php';
$handler->display->display_options['header']['php']['empty'] = FALSE;
$handler->display->display_options['header']['php']['php_output'] = '<?php

  $view = views_get_current_view();
  $view->get_total_rows = TRUE;
  $total_items = $view->query->pager->get_total_items();
  print \'<h2>\'.$view->total_rows.\' Users Found</h2>\';
  
 ?>
';
/* No results behavior: Global: PHP */
$handler->display->display_options['empty']['php']['id'] = 'php';
$handler->display->display_options['empty']['php']['table'] = 'views';
$handler->display->display_options['empty']['php']['field'] = 'php';
$handler->display->display_options['empty']['php']['empty'] = FALSE;
$handler->display->display_options['empty']['php']['php_output'] = 'There are no results for those e-mail addresses';
/* Field: User: Uid */
$handler->display->display_options['fields']['uid']['id'] = 'uid';
$handler->display->display_options['fields']['uid']['table'] = 'users';
$handler->display->display_options['fields']['uid']['field'] = 'uid';
$handler->display->display_options['fields']['uid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['uid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['uid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['uid']['alter']['external'] = 0;
$handler->display->display_options['fields']['uid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['uid']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['uid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['uid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['uid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['uid']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['uid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['uid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['uid']['alter']['html'] = 0;
$handler->display->display_options['fields']['uid']['element_label_colon'] = 1;
$handler->display->display_options['fields']['uid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['uid']['hide_empty'] = 0;
$handler->display->display_options['fields']['uid']['empty_zero'] = 0;
$handler->display->display_options['fields']['uid']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['uid']['link_to_user'] = 1;
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['label'] = 'Username';
$handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['name']['alter']['external'] = 0;
$handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['name']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['name']['alter']['word_boundary'] = 0;
$handler->display->display_options['fields']['name']['alter']['ellipsis'] = 0;
$handler->display->display_options['fields']['name']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['name']['alter']['trim'] = 0;
$handler->display->display_options['fields']['name']['alter']['html'] = 0;
$handler->display->display_options['fields']['name']['element_label_colon'] = 1;
$handler->display->display_options['fields']['name']['element_default_classes'] = 1;
$handler->display->display_options['fields']['name']['hide_empty'] = 0;
$handler->display->display_options['fields']['name']['empty_zero'] = 0;
$handler->display->display_options['fields']['name']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['name']['link_to_user'] = 1;
$handler->display->display_options['fields']['name']['overwrite_anonymous'] = 0;
$handler->display->display_options['fields']['name']['format_username'] = 1;
/* Field: User: E-mail */
$handler->display->display_options['fields']['mail']['id'] = 'mail';
$handler->display->display_options['fields']['mail']['table'] = 'users';
$handler->display->display_options['fields']['mail']['field'] = 'mail';
$handler->display->display_options['fields']['mail']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['mail']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['mail']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['mail']['alter']['external'] = 0;
$handler->display->display_options['fields']['mail']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['mail']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['mail']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['mail']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['mail']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['mail']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['mail']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['mail']['alter']['trim'] = 0;
$handler->display->display_options['fields']['mail']['alter']['html'] = 0;
$handler->display->display_options['fields']['mail']['element_label_colon'] = 0;
$handler->display->display_options['fields']['mail']['element_default_classes'] = 1;
$handler->display->display_options['fields']['mail']['hide_empty'] = 0;
$handler->display->display_options['fields']['mail']['empty_zero'] = 0;
$handler->display->display_options['fields']['mail']['hide_alter_empty'] = 1;
/* Field: User: Last login */
$handler->display->display_options['fields']['login']['id'] = 'login';
$handler->display->display_options['fields']['login']['table'] = 'users';
$handler->display->display_options['fields']['login']['field'] = 'login';
$handler->display->display_options['fields']['login']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['login']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['login']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['login']['alter']['external'] = 0;
$handler->display->display_options['fields']['login']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['login']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['login']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['login']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['login']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['login']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['login']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['login']['alter']['trim'] = 0;
$handler->display->display_options['fields']['login']['alter']['html'] = 0;
$handler->display->display_options['fields']['login']['element_label_colon'] = 1;
$handler->display->display_options['fields']['login']['element_default_classes'] = 1;
$handler->display->display_options['fields']['login']['hide_empty'] = 0;
$handler->display->display_options['fields']['login']['empty_zero'] = 0;
$handler->display->display_options['fields']['login']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['login']['date_format'] = 'raw time ago';
/* Field: User: Created date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'users';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['created']['alter']['external'] = 0;
$handler->display->display_options['fields']['created']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['created']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['created']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['created']['alter']['trim'] = 0;
$handler->display->display_options['fields']['created']['alter']['html'] = 0;
$handler->display->display_options['fields']['created']['element_label_colon'] = 1;
$handler->display->display_options['fields']['created']['element_default_classes'] = 1;
$handler->display->display_options['fields']['created']['hide_empty'] = 0;
$handler->display->display_options['fields']['created']['empty_zero'] = 0;
$handler->display->display_options['fields']['created']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['created']['date_format'] = 'raw time ago';
/* Field: User: Active */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'users';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['status']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['status']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['status']['alter']['external'] = 0;
$handler->display->display_options['fields']['status']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['status']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['status']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['status']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['status']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['status']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['status']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['status']['alter']['trim'] = 0;
$handler->display->display_options['fields']['status']['alter']['html'] = 0;
$handler->display->display_options['fields']['status']['element_label_colon'] = 1;
$handler->display->display_options['fields']['status']['element_default_classes'] = 1;
$handler->display->display_options['fields']['status']['hide_empty'] = 0;
$handler->display->display_options['fields']['status']['empty_zero'] = 0;
$handler->display->display_options['fields']['status']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: User: Direct Referer */
$handler->display->display_options['fields']['field_direct_referer']['id'] = 'field_direct_referer';
$handler->display->display_options['fields']['field_direct_referer']['table'] = 'field_data_field_direct_referer';
$handler->display->display_options['fields']['field_direct_referer']['field'] = 'field_direct_referer';
$handler->display->display_options['fields']['field_direct_referer']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_direct_referer']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_direct_referer']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_direct_referer']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_direct_referer']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_direct_referer']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_direct_referer']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_direct_referer']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_direct_referer']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_direct_referer']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['field_direct_referer']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_direct_referer']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_direct_referer']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_direct_referer']['element_label_colon'] = 1;
$handler->display->display_options['fields']['field_direct_referer']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_direct_referer']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_direct_referer']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_direct_referer']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['field_direct_referer']['field_api_classes'] = 0;
/* Field: User: Refered by */
$handler->display->display_options['fields']['field_refered_by']['id'] = 'field_refered_by';
$handler->display->display_options['fields']['field_refered_by']['table'] = 'field_data_field_refered_by';
$handler->display->display_options['fields']['field_refered_by']['field'] = 'field_refered_by';
$handler->display->display_options['fields']['field_refered_by']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_refered_by']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_refered_by']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_refered_by']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_refered_by']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_refered_by']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_refered_by']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_refered_by']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_refered_by']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_refered_by']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['field_refered_by']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_refered_by']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_refered_by']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_refered_by']['element_label_colon'] = 1;
$handler->display->display_options['fields']['field_refered_by']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_refered_by']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_refered_by']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_refered_by']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['field_refered_by']['field_api_classes'] = 0;
/* Field: Global: PHP */
$handler->display->display_options['fields']['php']['id'] = 'php';
$handler->display->display_options['fields']['php']['table'] = 'views';
$handler->display->display_options['fields']['php']['field'] = 'php';
$handler->display->display_options['fields']['php']['label'] = 'Returning?';
$handler->display->display_options['fields']['php']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['php']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['php']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['php']['alter']['external'] = 0;
$handler->display->display_options['fields']['php']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['php']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['php']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['php']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['php']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['php']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['php']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['php']['alter']['trim'] = 0;
$handler->display->display_options['fields']['php']['alter']['html'] = 0;
$handler->display->display_options['fields']['php']['element_label_colon'] = 0;
$handler->display->display_options['fields']['php']['element_default_classes'] = 1;
$handler->display->display_options['fields']['php']['hide_empty'] = 0;
$handler->display->display_options['fields']['php']['empty_zero'] = 0;
$handler->display->display_options['fields']['php']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['php']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php']['php_output'] = '<?php
$created = $row->created;
$lastlogin = $row->login;

if( $lastlogin - $created < 43200){
print \'Have not returned\';
}
else{
print \'Returned\';
}
//print $row->created;
//print $row->login;

//print $lastlogin - $created;
?>';
$handler->display->display_options['fields']['php']['use_php_click_sortable'] = '2';
$handler->display->display_options['fields']['php']['php_click_sortable'] = '';
/* Field: User: Edit link */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'users';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = 'Edit';
$handler->display->display_options['fields']['edit_node']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['external'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['edit_node']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['edit_node']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['trim'] = 0;
$handler->display->display_options['fields']['edit_node']['alter']['html'] = 0;
$handler->display->display_options['fields']['edit_node']['element_label_colon'] = 1;
$handler->display->display_options['fields']['edit_node']['element_default_classes'] = 1;
$handler->display->display_options['fields']['edit_node']['hide_empty'] = 0;
$handler->display->display_options['fields']['edit_node']['empty_zero'] = 0;
$handler->display->display_options['fields']['edit_node']['hide_alter_empty'] = 1;
/* Sort criterion: User: Created date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'users';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Contextual filter: User: Uid */
$handler->display->display_options['arguments']['uid']['id'] = 'uid';
$handler->display->display_options['arguments']['uid']['table'] = 'users';
$handler->display->display_options['arguments']['uid']['field'] = 'uid';
$handler->display->display_options['arguments']['uid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['uid']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['uid']['break_phrase'] = 1;
$handler->display->display_options['arguments']['uid']['not'] = 0;
/* Filter criterion: User: Refered by (field_refered_by) */
$handler->display->display_options['filters']['field_refered_by_value']['id'] = 'field_refered_by_value';
$handler->display->display_options['filters']['field_refered_by_value']['table'] = 'field_data_field_refered_by';
$handler->display->display_options['filters']['field_refered_by_value']['field'] = 'field_refered_by_value';
$handler->display->display_options['filters']['field_refered_by_value']['group'] = 1;
$handler->display->display_options['filters']['field_refered_by_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_refered_by_value']['expose']['operator_id'] = 'field_refered_by_value_op';
$handler->display->display_options['filters']['field_refered_by_value']['expose']['label'] = 'Refered by';
$handler->display->display_options['filters']['field_refered_by_value']['expose']['operator'] = 'field_refered_by_value_op';
$handler->display->display_options['filters']['field_refered_by_value']['expose']['identifier'] = 'field_refered_by_value';
$handler->display->display_options['filters']['field_refered_by_value']['expose']['reduce'] = 0;
/* Filter criterion: User: Active */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'users';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 'All';
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['exposed'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['operator_id'] = '';
$handler->display->display_options['filters']['status']['expose']['label'] = 'Active';
$handler->display->display_options['filters']['status']['expose']['use_operator'] = FALSE;
$handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['expose']['multiple'] = FALSE;

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'are-they-registered';

$views[$view->name] = $view;
return $views;
}

?>