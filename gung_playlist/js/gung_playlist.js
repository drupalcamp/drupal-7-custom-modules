Drupal.behaviors.playlistSelectBox = {
    attach: function(context, settings) {

        jQuery('#playlist_form .form-item-playlist select').change(function() {

            var selectedValue = jQuery(this).find(":selected").val();
            jQuery("#video_info SELECT").selectBox('value', 0);

            if (!Drupal.settings.authenticated_user) {
                jQuery('.ajax-register-links a[title="Login"]').trigger('click');
            } else {
                //1 = trigger new playlist form
                if (selectedValue == 4) {
                    //Switch ACtive Video_nid
                    var link = jQuery('#playlist_form  a.ctools-use-modal');
                    var url = link.attr('href');
                    if (activeVideoNID) {

                        var newUrl = url + '' + 'video_nid=' + activeVideoNID;
                        link.attr('href', newUrl);
                    }

                    //Re-attach cTools Modal information becase we changed the href
                    link.removeClass('ctools-use-modal-processed');
                    link.unbind('click');
                    Drupal.behaviors.ZZCToolsModal.attach();

                    link.trigger('click');

                    console.log('selectedValue ==' + selectedValue);

                } else if (selectedValue != 0) {
                    //Run AJAX request for adding to playlist
                    var playlist_nid = selectedValue;

                    if (!activeVideoNID) {
                        var video_nid = jQuery('#active_video_nid').text();
                        if (!video_nid) {}
                        activeVideoNID = video_nid;
                    }

                    jQuery.ajax({
                        url: '?q=gung_playlist/add_to_playlist&playlist_nid=' + playlist_nid + '&video_nid=' + activeVideoNID,

                        beforeSend: function() {
                            //Show a throbber
                        },

                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            console.log('an error occured');

                            jQuery('#modal-title').html('Added to Playlist');
                            jQuery('#modal-content').html('<div class="messages status"> <h2 class="element-invisible">Status message</h2> The video has been added to the <a href="user/me/playlists">playlist</a> </div>').scrollTop(0);
                            Drupal.attachBehaviors();
                            console.log(XMLHttpRequest);
                            console.log(textStatus);
                            console.log(errorThrown);
                            location.reload();
                        },
                        success: function(data) {
                            console.log(data);
                            Drupal.behaviors.simple_loader.hideLoader();
                            Drupal.CTools.Modal.show('ctools-ajax-register-style');
                            jQuery('#modal-title').html('Added to Playlist');
                            jQuery('#modal-content').html(data.message).scrollTop(0);
                            Drupal.attachBehaviors();
                            //location.reload();
                        }

                    });
                }
            } // line 13: END
        });

    }
};

Drupal.behaviors.playlistContentBoard = {
    attach: function(context, settings) {
        var $container = jQuery('#playlist_board_wrapper');
        if (!$container.length) {
            return;
        }

        $container.imagesLoaded(function() {
            jQuery('.playlist_content_item', $container).removeClass('new');
            $container.masonry({
                itemSelector: '.playlist_content_item',
                isAnimated: true,
                isFitWidth: true,
                //cornerStampSelector: '.corner-stamp'
            });
        });

        jQuery.autopager({
            content: ".playlist_content_item",
            link: '#next',
            appendTo: "#playlist_board_wrapper",
            start: function() {
                jQuery('.playlist_content_item', $container).removeClass('new');
                jQuery('#playlistsLoader').fadeIn();
            },
            load: function(current, next) {
                var $newElems = jQuery('.new', $container).css({ opacity: 0 });
                $newElems.imagesLoaded(function() {
                    $newElems.css({ opacity: 1 });
                    $container.masonry('appended', $newElems, true);
                });
                jQuery('#playlistsLoader').hide();
            }
        });



    }
}