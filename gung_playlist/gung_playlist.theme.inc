<?php

/**
 * Returns image field formatter.
 *
 * @param $variables
 *   An associative array containing:
 *   - items: An array of image data.
 *   - list-style: An array of image data.
 *
 * @ingroup themeable
 */
function theme_gung_playlist_list($variables) {
	global $theme;
	drupal_add_js(drupal_get_path('module', 'gung_playlist')  .'/js/gung_playlist_field_formatter.js');
	//drupal_add_css(drupal_get_path('theme', 'gung_playlist') );

	$playlist_video_ids = array();
	$thumb_items = array();

	$playlist = $variables['playlist'];
	$items = $variables['items'];

	foreach($items as $item) {

		$video_node = node_load($item['nid']);
		if (!$video_node){
			$video_node = new stdClass();
			$video_node->ajax_vid = NULL;
		}
		$video_node->ajax_vid = true;

		if(arg(0) == 'user' || arg(0) == 'forum') {
			$video_node->ajax_vid = false;
		}

		//If AJAX vid, than use actual path, for not, jump to Video Player page and play there
		if($video_node->ajax_vid || !isset($playlist)) {
			$path = url('node/' . $video_node->nid);
		} else {

			$path = url('node/' . $playlist->nid, array('query' => array( 'id' => $video_node->nid) ));

		}

		if(isset($video_node->nid)) {
			//$thumb_items[] = ($theme == 'gungmobile' ? '<div class="twelve mobile-twelve columns playlist-item">'. render(node_view($video_node, 'teaser')). '</div>' : render(node_view($video_node, 'teaser')));
			$node_view_obj = node_view($video_node, 'teaser');
			$thumb_items[] = ($theme == 'gungmobile' ? '<div class="twelve mobile-twelve columns playlist-item">'
			. render($node_view_obj). '</div>' : render($node_view_obj));

		}
	}

	//jCarousel Theme
	$options = array (
			'skin' => 'gungwang',
			'scroll' => 5,
			'animation' => 'slow',
			'easing' => 'swing',
			'wrap' => null,
			'initCallback' => 'carousel_initCallback',
	);

	$thumb_view = theme('jcarousel', array('items' => $thumb_items, 'options' => $options, ));

	$output = '<h2 class="playlist-results-title block-title" >' . $playlist->title . '</h2>';
	if($theme == 'gungmobile') {
		// dpm(get_defined_vars(), 'defined vars');
		global $base_url;
		$title = $variables['playlist']->title;
		$output .= '<div class="breadcrumb">'. l(t('Playlists'), $base_url. '/playlists'). '&nbsp > &nbsp'. $title. '</div><div id="playlist-results" class="videos-thumb-view">' . implode($thumb_items) . '</div>';
	}
	else {
	$output	.= '<div style="margin-left: 10px; float: left; margin-top: 2px;">' . gung_global_short_add_this_button() . '</div>';
  	$output .= '<div id="playlist-results" class="videos-thumb-view">' . $thumb_view . '</div>';
  }

  return $output;
}

function theme_gung_playlist_content_list_item($variables) {
	$playlist_node = $variables['node'];
	$show_experts = $variables['list_featured_experts'];
	$preview_number = $variables['preview_number'];


	$playlist_video_nodes = field_get_items('node', $playlist_node, 'gung_video_node_references');
  $firstnid = $playlist_video_nodes['0']['nid'];

	$count = count($playlist_video_nodes);
	if($count == 0) {
		$playlist_video_nodes = array();
	}

	$videos_preview = array();
	$experts = array();

	//Show Thumbnail and render it
	$cover_image = field_get_items('node', $playlist_node, 'field_playlist_cover_image');

	if(!empty($cover_image)) {
		$f_cover_image = field_view_field('node', $playlist_node, 'field_playlist_cover_image', 'default');
		$thumb = render($f_cover_image);
	}

	$play = l('<div class="play tooltip"> </div>', 'node/' . $playlist_node->nid, array('html' => true, 'query' => array('id' => $firstnid ), 'fragment' => 'playvid/'.$firstnid));

	foreach($playlist_video_nodes as $item) {
		$video_node = node_load($item['nid']);
		if(!isset($thumb)) {
			$video_node->path = url('node/' . $playlist_node->nid);
			$thumb =  render(node_view($video_node, 'teaser'));
		}
		if(count($experts) <= 2) {
		  $expert = field_get_items('node', $video_node, 'field_video_expert');
		  $expert_node = node_load($expert[0]['nid']);
	    $experts[$expert_node->nid] = l($expert_node->title, 'node/' . $expert_node->nid, array('html' => true, 'query' => array('id' => $firstnid ),'fragment' => 'playvid/'.$firstnid));
		}

		if(count($videos_preview) < $preview_number) {

  	  $title = l($video_node->title, 'node/' . $playlist_node->nid, array('html' => true, 'query' => array('id' => $video_node->nid ),'fragment' => 'playvid/'.$video_node->nid));
  	  $videos_preview[] =  array($title);
		}
	}

	$title = l( '<b>Play All ' . $count . ' Videos </b>', 'node/' . $playlist_node->nid, array('html' => true, 'query' => array('id' => $firstnid ),'fragment' => 'playvid/'.$firstnid, 'attributes' => array('class' => array('play_all') )));
	$videos_preview[] =  array($title);



	$output = '<div class="playlist_content_item new">';
	$output .= '<div class="playlist_content_item_thumb">';
	$output .= $play;
	$output .= $thumb;
	$output .= '</div>';

	$output .= '<div class=" playlist_content_item_text">';
	$output .= '<div class="title">' . l($playlist_node->title, 'node/' . $playlist_node->nid, array('html' => true, 'query' => array('id' => $firstnid ),'fragment' => 'playvid/'.$firstnid)) . '</div>';
	//$output .= '<div class="right count">' . $count . ' Videos</div>';

	$output .= '<div class="playlist_list">' . theme('table', array( 'rows' => $videos_preview)) . '</div>';

	if($show_experts) {
	  $output .= '<div class="experts_featured"><span style="font-weight: bold;">Featured Experts: </span>  ' . implode(', ', $experts) . '</div>';
	}

	$output .= '</div>';
	$output .= '</div>';

	return $output;

}
