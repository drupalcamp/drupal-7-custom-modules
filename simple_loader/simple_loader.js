(function ($) {

  Drupal.behaviors.simple_loader = {
      attach: function (context, settings){
	  
	//  console.log('Simple Loader Init');
	  
	   },
	   showLoader: function(){
	     $('#page-simple-loader').fadeIn();
		 console.log('Show Loader');
	   },
	   hideLoader: function(){
	       $('#page-simple-loader').fadeOut();
	   },
	   timeShowLoader: function(){
	   $('#page-simple-loader').fadeIn();
       setTimeout(function(){ $('#page-simple-loader').fadeOut(); }, 6000);
	   
	   },
	   timeHideLoader: function(){}, 
	   
	     };
		 
		 
}(jQuery));