<?php

/**
 * @file
 * Presents users with an option to cancel a recurring subscription created with commerce recurring framework and commerce license
 */

/**
 * Implements hook_perm().
 *
 * Make cancellations a separate permission, in case we need that level of
 * granularity for a particular store.
 */
function commerce_license_manager_permission() {
  return array(
    'cancel own subscription renewals' => array(
      'title' => t('Cancel Own Subscription Renewals'),
    ),
  );
}

function commerce_license_manager_acl_nids(){
  global $user;
  $query = db_query("SELECT GROUP_CONCAT(CAST(an.nid AS CHAR) SEPARATOR ',') FROM {acl_user} au INNER JOIN {acl_node} an ON au.acl_id = an.acl_id WHERE an.grant_view = 1 AND au.uid = :uid", array(':uid' => $user->uid));
  $result = $query->fetchObject();
  return reset($result);
}

/**
 * Implements hook_menu().
 */
function commerce_license_manager_menu() {
  $items['cancel-subscription-renewals'] = array(
    'title'            => 'Cancel Subscription Renewals',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('commerce_license_manager_cancel_renewals_form'),
    'access callback' => 'user_access',
    'access arguments' => array('cancel own subscription renewals'),
    'description'      => 'Cancel subscription renewals',
    'type'             => MENU_CALLBACK,
  );

  $items['orders/me'] = array(
    'title' => 'My Orders',
    'description' => 'Manage my orders',
    'page callback' => 'commerce_license_manager_mystats',
    'access callback' => TRUE,
  //  'access arguments' => array('cancel own subscription renewals'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

function commerce_license_manager_mystats(){
  global $user;

  if(!user_is_logged_in()){
    drupal_set_message('Please Login Below');
    drupal_goto('user/login', array('query' => array('destination' => 'orders/me')));
  }

  $uid = $user->uid;
  $orders = commerce_order_load_multiple(array(), array('uid' => $user->uid));

  if(isset($orders)){

  //<a href="/user/me/orders">My Orders</a> |
  //<a href="/user/me/cards">My Cards on File</a>

    $content_list = acl_extras_premium_content_list($uid);

     $order_history = views_embed_view('commerce_user_orders', 'order_page', $uid);
     $my_subscriptions = views_embed_view('my_subscriptions', 'page', $uid);

    $output = '';

    if (in_array('premium member', $user->roles)) {

      //$output .= '<a href="/user/me/my-subscriptions">| My Membership</a>';

      $output .= <<<EOD
      <div class="orders-p-wrapper">
       <h2>Premium Content Subscriptions</h2>
       $my_subscriptions
       </div>
EOD;
    }
    else{
      $output .= <<<EOD
      <div class="orders-p-wrapper">
       <h2><span class="premium-title">Click Here to Watch Video Now:</span></h2> $content_list
      </div>
      <div class="orders-p-wrapper">
       <h2>Order History</h2>
       $order_history
       </div>
EOD;
    }


//  $output .= '</p>';
  }
  else{
    $output = '<div class="orders-p-wrapper"><p>You do not have any current orders</p></div>';
  }
  return $output;
}

/**
 * Implements hook_views_api().
 */
function commerce_license_manager_views_api() {
  return array(
    'api'  => 3,
    'path' => drupal_get_path('module', 'commerce_license_manager') . '/views',
  );
}


/**
 *
 */
function commerce_license_manager_cancel_renewals_form($form, &$form_state) {
  global $user;

  // We'll redirect to these paths in various conditions
  $user_path = 'user';
  $my_subscriptions_path = 'user/' . $user->uid . '/my-subscriptions';

  // Confirm the user is logged in, bail to login page otherwise.
  if (user_is_anonymous()) {
    drupal_set_message(t("You must be logged in to cancel your subscription renewals."));
    drupal_goto($user_path);
  }

  // Confirm we actually have a subscription (recurring entity) to work with.
  // Bail to the My Subscriptions list otherwise.
  $id = arg(1);
  if (!empty($id)) {
    $id = trim(check_plain($id));
    $recurring_entity = commerce_recurring_load($id);
    if (!$recurring_entity) {
      drupal_set_message(t("Unable to find subscription ") . $id);
      drupal_goto($my_subscriptions_path);
    }
  }

  // Confirm this user can act on this recurring entity
  if (!_commerce_license_manager_can_cancel_subscription($recurring_entity)) {
    drupal_set_message(t("Permission denied for subsciption ") . $id);
    drupal_goto($my_subscriptions_path);
  }

  // If we get to here, we're allowed to cancel the subscription.
  $form['id'] = array(
    '#value' => $id,
  );

  $form = confirm_form($form,
    t('Are you sure you want to cancel your subscription renewal?'),
    $my_subscriptions_path,
    t('This action cannot be undone.'),
    t('Cancel subscription renewal'),
    t('Return to My Subscriptions')
  );

  return $form;
}


/**
 * Gung : addd email functionality when a user cancels the submiscription
 * Date : January 4, 2017
 * Update: call function commerce_license_manager_email_cancel()
 */
function commerce_license_manager_cancel_renewals_form_submit($form, &$form_state) {
  global $user;
  if ($form_state['values']['confirm']) {
    $recurring_entity_id = $form['id']['#value'];
    $recurring_entity = commerce_recurring_load($recurring_entity_id);
    //var_dump($recurring_entity); exit();

    commerce_recurring_stop_recurring($recurring_entity);
    if(commerce_license_manager_email_cancel($user->mail)){
      watchdog('commerce', "The user " . $user->mail ." cancelled the membership subscription.");
    }

    // if(commerce_license_manager_mailto_office($user, $recurring_entity)){
    //   watchdog('commerce', "Send email to office@gungwang.com:  The user " . $user->mail ." cancelled the membership subscription.");
    // }
  }

  $my_subscriptions_path = 'user/' . $user->uid . '/my-subscriptions';
  $form_state['redirect'] = $my_subscriptions_path;
}


/*
 * Determines if the current user can assess the specified recurring entity
 */
function _commerce_license_manager_can_cancel_subscription($recurring_entity) {
  global $user;

  return user_access('edit any commerce_recurring entity', $user) ||
         user_access('cancel own subscription renewals', $user) && $recurring_entity->uid == $user->uid;
}

/*
function commerce_license_manager_mailto_office(&$user, &$recurring_entity){
  //$to = 'office@gungwang.com';
  $to = 'gung@gungwang.com';
  $from = 'info@gungwang.com';
  $language = user_preferred_language($user);
  $subject = "The user " . $user->mail ." cancelled the membership subscription";
  $body = "The user " . $user->mail ." cancelled the membership subscription.<br>";
  $body .= "Canceled Date:" . date("F j, Y, g:i a");
  $headers = array(
    'MIME-Version' => '1.0',
    'Content-Type' => 'text/html; charset=UTF-8; format=flowed; delsp=yes',
    'Content-Transfer-Encoding' => '8Bit',
    'X-Mailer' => 'Drupal',
  );

  $params = array(
    'body' => $body,
    'subject' => $subject,
    'headers' => $header
  );

  $message = drupal_mail('commerce_license_manager', 'notice', $to, $language, $params, $from, FALSE);
   // Only add to the spool if sending was not canceled.
  if ($message['send']) {
    return true;
  }

  return false;
}
*/

/**
 * Gung : addd email functionality when a user cancels the submiscription
 * Date : January 4, 2017
 * Update: August, 20217:  Email office@gungwang.com when a order cancelded.
 * New  : function to send email.
 */
function commerce_license_manager_email_cancel($to, $destination='/go-premium-to-view'){
  $host = $_SERVER['HTTP_HOST'];

  $from = variable_get('site_mail', 'info@gungwang.com');
  $Cc = 'office@gungwang.com';
  $language = language_default();

  list($subject, $body) = commerce_license_manager_get_email_subject_content($to, $host, $destination);
  $content = commerce_license_manager_html_email_content($to, $host, $destination, $body);

  $email_body = "<html><header></header><body style=\"margin: 0; padding: 0;\">$content</body>";

  $headers = array(
    'Cc' => $Cc,
    'MIME-Version' => '1.0',
    'Content-Type' => 'text/html; charset=UTF-8; format=flowed; delsp=yes',
    'Content-Transfer-Encoding' => '8Bit',
    'X-Mailer' => 'Drupal',
  );

  $headers['From'] = $headers['Sender'] = $headers['Return-Path'] = $from;

  $my_module = 'verify_email';
  $my_mail_token = microtime();

  $message = array(
    'id' => $my_module.'_'.$my_mail_token,
    'from' => $from,
    'to' => $to,
    'subject' => $subject,
    'body' => $email_body,
    'headers' => $headers,
  );

  $system = drupal_mail_system($my_module, $my_mail_token);
  if ($system->mail($message)) {
    return true;
  } else {
    return false;
  }
}

/**
 * Gung : addd email functionality when a user cancels the submiscription
 * Date : January 4, 2017
 * New  : function to send email.
 */
function commerce_license_manager_get_email_subject_content($to, $host, $destination){
  $subject = $content ="";
  $link = "<a href=\"http://$host/$destination\">Subscribe to Premium</a>";
      $subject = "Cancelled subscription Renewals at Gung Wang.";
      $content = "Membership subscription has been cancelled.<br><b>User Account: $to</b> <br><br>If you would like to keep access to Premium content, you can subscribe here again: $link"       ;
  return array($subject, $content);
}

/*
 * Date : January 4, 2017
 * Purpose: the content of the email
 * Params:  $email, $site, $type, $host, $destination
 */
function commerce_license_manager_html_email_content($email, $host, $destination, $content)
{

  $host = trim($host);

  $table_header1 = '<img src="/sites/all/themes/gungwang/images/newsletter/gungwang-email-logo-with-space.jpg" '.
      'class="CToWUd" border="0px;" style="width:240px;">';

  $table_html_header = '<table width="100%" cellspacing="0" cellpadding="0" border="0" height="100%">';
  $table_html_header .= "<tr><td style=\"text-align:center;\">$table_header1</td></tr></table>";

  $table_cr = '(C) 2016 <a href="/" target="_blank">gungwang.com</a> '.
      'All rights reserved | <a href="/privacy-policy" target="_blank">Privacy policy</a> '.
      '| <a href="/terms-use" target="_blank">Terms</a>' ;

  // $params_str = "id=$uid&email=$email&url=/$destination";
  $http = "http://$host/$destination";

  $css_signup = 'style="padding:8px;border:1px solid #ec3035;border-radius:4px;font-size:16px;font-weight:bold;
          color:#fff; background-image: linear-gradient(to bottom, #E61F25 0%, #B5151A 80%);
          display:inline-block;text-align:center;width:150px;text-decoration:none;"';
  //$css_p14 = 'style="margin:20px 10px 40px 0; font-size:1.3em;"';
  $css_p12 = 'style="font-size:1.2em;padding-top:10px;"';

  //$str = "<h1 style=\"color:#fa2129;text-align:center;\">Welcome to gungwang</h1>";
  $str = '<table width="100%" height="100%"><tr><td>';
  $str .= "<h3 style='padding:0;font-weight:normal;font-size:1.3em;'>$content</h3>";
  $str .= '</td></tr><tr><td>';

  $str .= "<p $css_p12>For questions about this list, please contact: office@gungwang.com</p>";
  $str .= '</td></tr></table>';

  $table_html = '<table width="100%" cellspacing="0" cellpadding="0" border="0" height="100%">';
  // $table_html .= "<tr><td style=\"text-align:left;\" width=\"65%\">$table_header1</td>
  //                 <td style=\"text-align:center;\" width='35%' >$table_header2</td></tr>";
  $table_html .= "<tr><td>$str</td></tr>";
  $table_html .= "<tr><td style=\"height:145px; background-image:url('/sites/all/themes/gungwang/images/newsletter/footer_bg.jpg'); background-repeat:no-repeat; background-size: 100% 100%; text-align:center\"></td></tr>";
  $table_html .= "<tr><td style=\"text-align:center; background-color:#efefef;\">$table_cr</td></tr></table>";
  return $table_html_header. $table_html;
}
