<?php

namespace Drupal\commerce_affiliates_tracking\Component\CommerceAffiliatesTrackingProductCollection;

class CommerceAffiliatesTrackingProductCollection extends ArrayObject{

	public $order_id;
	public $products = array();

	public function __construct ($order_id){
		$this->order_id = order_id;
		$this-> addProductsOrder($order_id); 


	}


	public function addProductsOrder($order){

		if (!($order instanceof EntityMetadataWrapper)) {
			$order = entity_metadata_wrapper('commerce_order', $order);
		}
		if (module_exists('commerce_tax')) {
			foreach (commerce_tax_rates() as $name => $tax_rate) {
				if ($tax_rate['price_component']) {
					$tax_component = commerce_price_component_load($order->commerce_order_total->value(), $tax_rate['price_component']);
					// Some taxes may not have been applied.
					if (isset($tax_component[0]['price']['amount'])) {
						$tax_sum += commerce_currency_amount_to_decimal($tax_component[0]['price']['amount'], $tax_component[0]['price']['currency_code']);
					}
				}
			}
		}
		if (module_exists('commerce_shipping')) {
			foreach ($order->commerce_line_items as $item) {
				if ($item->type->value() == 'shipping') {
					$shipping += commerce_currency_amount_to_decimal($item->commerce_total->amount->value(), 
					                                           $item->commerce_total->currency_code->value());
				}
			}
		}

		
		foreach ($order->commerce_line_items as $line_item_wrapper) {
			$this->addProduct ($line_item_wrapper->commerce_product);
		}

		//return products in JASON object
		public function getProductJSON(){
			// create a new json object, loop the products itself and call method, getProductInfo("json"), and create json code for each item, return the whole object.
		}

		public function getProductArray(){
			//getProductInfo("phparray")
			return $this->products;
		}

		public function addProduct($product){
			//if $product is object : just check is a product , append the item to it
			//if number: create  a new product project from productID and append a item to the product itself
			
			$products []=$product;
		}

		public function getAffiliates(){
			// each product has it own CJ affiliate property 
			// for each product, assign a a property 
			// the hard code to call _shareasale
			return array('commerce_affiliates_tracking_shareasale');
			
			
			)
			
		}

		public function generateAllAffilliates($products=null){
			//lookup all affiliates defined in sub modules. Pass $products to submodules, get specific affilliate tracking back, save to itself $this->affiliate1 etc etc.
			if ($products == '') {
				$products = $this->products;
			}
			
			
			$html_output = '';
			foreach (getAffiliates() as $affliate ){
				$html_output .= $affiliate($products);
			}
			
			// add data into session;
			session_cache_set('CommerceAffiliateTrackingCode',$html_output);

			
			return $html_output;
		}

		public function getAffiliatesJSON(){
			// returns all JSON snippets to render on page
		}


	}

