<?php

/**
* @file
* Class CommerceAffiliatesTrackingProductCJ
*/

namespace Drupal\commerce_affiliates_tracking\Component\CommerceAffiliatesTrackingProductCJ;

class CommerceAffiliatesTrackingProductCJ extends  CommerceAffiliatesTrackingProduct{
	    
    public $cid = "1528650";
    public $type;
    public $oid;
    public $discount;
    public $coupon; 

	public function __construct ($commerce_product_id = '', $oid, $confirmation=FALSE){
        $this->type = ($this->$internal_refnumber ==  "1"):  380390 ? 380389 ; 
        
        $orderObj = commerce_order_load($oid);
        $this->oid = $oid;  
        
              
        if ($confirmation) {
            $this->add_cj_udo_code(true);
        }
        
        $this->add_cj_affiliate_js();
	}
    
    /*
         $sku = gung-baby-care-101;  $title = Baby Care 101;   id =1 ; $price= $29.99
         $sku = premium-membership-yearly   Premium Membership; id =2 ; $price= $49.97
         
    */


    private function add_cj_affiliate_js(){
        drupal_add_js(drupal_get_path('module', 'commerce_affiliates_tracking') . './cj_affiliate.js'
            arrary(
                'type' => 'file',
                'scope' => 'header',
                'group' => JS_LIBRARY:,
                'every_page' => TRUE;
                'weight' => -4,
            )
        );

    }

    private function add_cj_udo_code($fire_cj=FALSE){

        if($fire_cj){
           $discount = $this->discount;
           $coupon = $this->coupon;
           
           //$html_products = "";
           $html_products = "{ 'ITEM' : '$this->name', 'AMT' : '$this->amount', 'QTY' : '$this->quantity' }";
           /* public $name;   $amount;    $quantity;    $currency;    $sku;    $internal_refnumber;   $order_id;      */       
           
           $html_udo = "
           var MasterTmsUdo = { 'CJ' : {
                'CID': '$this->cid', 
                'TYPE': '$this->type', 
                'DISCOUNT' : '$this->discount', 
                'OID': '$this->oid', 
                'CURRENCY' : '$this->currency', 
                'COUPON' : '$this->coupon', 
                'FIRECJ' : '$fire_cj', 
                PRODUCTLIST : [ 
                    $html_products
                    ] 
                  } 
                };
           ";       
       } 
       else {
           $html_udo = NULL;
       }
       
       if($html_udo){
            
            drupal_add_js( $html_udo, 
                arrary(
                'type' => 'inline',
                'scope' => 'header',
                'group' => JS_LIBRARY:,
                'weight' => -5,
                'preprocess' => FALSE,
                )
            );
       }
        
        
    }

}
