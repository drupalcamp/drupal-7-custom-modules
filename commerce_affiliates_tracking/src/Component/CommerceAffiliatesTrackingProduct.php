<?php

/**
* @file
* Class CommerceAffiliatesTrackingProduct
*/

namespace Drupal\commerce_affiliates_tracking\Component\CommerceAffiliatesTrackingProduct;

class CommerceAffiliatesTrackingProduct {

	public $name;

	public $amount;
    
	public $quantity;

	public $currency;

	public $sku;

	public $internal_refnumber;

	public $order_id;

    /*
         $sku = gung-baby-care-101;  $title = Baby Care 101;   id =1 ; $price= $29.99
         $sku = premium-membership-yearly   Premium Membership; id =2 ; $price= $49.97
         
    */
    
	public function __construct ( $commerce_product_id = ''){
		watchdog('CommerceAffiliatesTrackingProduct','Constructor');
        if (isset($commerce_product_id)){
			watchdog('CommerceAffiliatesTrackingProduct','commerce_product_id');
			$this->setFromCommerceProduct($commerce_product_id, 1);
		}
	}

	private function setFromCommerceProduct($product_id, $quantity) {
		watchdog('CommerceAffiliatesTrackingProduct','setFromCommerceProduct');
		$product = commerce_product_load($product_id);
		$this->name = $product->title;
		$this->amount = $quantity;
		$this->internal_refnumber = $product->product_id;
		$this->sku = $product->sku;
		$this->currency = $product->commerce_price['und'][0]['currency_code'];
        $this->quantity = $quantity;
        
		$this->amount = commerce_currency_amount_to_decimal($product->commerce_price['und'][0]['amount'],$this->currency);
	}
    
	private function buildProductArray(){ 
		return array(
		'name' => $this->$name,
		'amount' => $this->$amount,
		'quantity' => $this->$quantity,
		'currency' => $this->$currency,
		'item number' => $this->$item_number
		);
	}

	public function getProductInfo($format) {
		switch($format){
		case 'array':
			return $this->buildProductArray();
			break;
		case 'JSON':
			return  json_encode($this->buildProductArray());
			break;
		}
	}

	public getTrackingAfflicateProperty(){
		// database table  OR 
		
	}

	public saveTrackingAfficateProperty(){
		// save the trackingID to database;
		
	}

}