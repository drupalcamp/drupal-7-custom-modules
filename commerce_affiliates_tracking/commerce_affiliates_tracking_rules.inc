<?php
/**
* @file
* Rules integration file
*/

/**
* Implements hook_rules_action_info().
*/
function commerce_affiliates_tracking_rules_action_info() {
	return array(
	// Add rules action which sends the transaction code.
	'commerce_affiliates_tracking_send_order' => array(
		'label' => t('Send order to data layer for CJ/shareASale tracking code'),
		'parameter' => array(
		'commerce_order' => array(
			'type' => 'commerce_order',
			'label' => t('Order in checkout'),
			),
		),
	'callback' => array(
		'execute' => 'commerce_affiliates_tracking_generate_affilates',
		),
	),
	/*
'ccommerce_affiliates_tracking_add_to_cart' => array(
	'label' => t('Send add to CJ/shareASale tracking code'),
	'parameter' => array(
	'commerce_order' => array(
		'type' => 'commerce_product',
		'label' => t('Product Added'),
	),
	),
	'callback' => array(
	'execute' => 'commerce_data_layer_add_to_cart',
	),
	),
*/

	);
}